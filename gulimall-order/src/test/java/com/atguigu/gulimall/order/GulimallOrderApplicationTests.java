package com.atguigu.gulimall.order;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnApplyEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Test
    public void sendMessageTest(){
        String msg = "hello world";
        //1.发送消息 如果发送的消息是个对象 我们会使用序列化机制 将对象写出去 对象必须实现Serializable接口
        for (int i = 0; i < 10; i++) {
            if (i%2 == 0){
                OrderReturnReasonEntity entity = new OrderReturnReasonEntity();
                entity.setId(1L);
                entity.setCreateTime(new Date());
                entity.setName("哈哈"+i);
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",entity);
            }else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",orderEntity);
            }
            log.info("{消息发送完成}",msg);
        }
    }
    /**
     * 1.如何创建Exchange Queue Binging
     *      1).使用AmqpAdmin进行创建
     *  2.如何收发消息
     */
    @Test
    public void createExchange() {
        //创建一个交换机
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
    }
    @Test
    public void createQueue(){
        //创建一个队列
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
    }
    @Test
    //建立绑定关系
    public void createBinding(){
        /**
         * String destination【目的地名字】
         * Binding.DestinationType destinationType【目的地类型】
         * String exchange【交换机】
         * String routingKey【路由键】
         * Map<String, Object> arguments【自定义参数】
         * 将exchange指定的交换机和destination目的地进行绑定 使用routingKey作为指定的路由键
         */
        Binding binding = new Binding(
                "hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java-exchange",
                "hello.java",
                null);
        amqpAdmin.declareBinding(binding);
    }

}
