package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-10-14 13:46
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
