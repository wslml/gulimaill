package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.PmsSkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author wsl
 * @email 526248876@qq.com
 * @date 2020-09-26 20:22:10
 */
@Mapper
public interface PmsSkuImagesDao extends BaseMapper<PmsSkuImagesEntity> {
	
}
