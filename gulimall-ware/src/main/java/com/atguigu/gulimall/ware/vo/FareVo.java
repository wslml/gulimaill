package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wsl
 * @create 2020-11-16 20:55
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
