package com.atguigu.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * @author wsl
 * @create 2020-11-19 18:30
 */

@Data
public class StockLockedTo {
    private Long id;//库存工作单的Id
    private StockDetailTo detail;//工作单详情
}
