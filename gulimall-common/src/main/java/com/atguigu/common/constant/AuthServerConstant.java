package com.atguigu.common.constant;

/**
 * @author wsl
 * @create 2020-11-04 15:31
 */
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PREFIX = "sms:code";
    public static final String LOGIN_USER = "loginUser";

}
