package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-10-31 14:53
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
