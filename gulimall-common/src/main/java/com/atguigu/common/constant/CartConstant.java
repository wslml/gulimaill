package com.atguigu.common.constant;

/**
 * @author wsl
 * @create 2020-11-10 10:29
 */
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    //配置临时cookie过期时间
    public static final int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;
}
