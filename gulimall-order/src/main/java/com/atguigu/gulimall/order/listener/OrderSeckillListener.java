package com.atguigu.gulimall.order.listener;

import com.atguigu.common.to.mq.SeckillOrderTo;
import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.service.OrderService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author wsl
 * @create 2020-11-19 23:07
 */
@Slf4j
@RabbitListener(queues = "order.seckill.order")
@Service
public class OrderSeckillListener {

    @Autowired
    OrderService orderService;

    @RabbitHandler
    public void listener(SeckillOrderTo seckillOrder, Channel channel, Message message) throws IOException {
        try{
            log.info("准备创建秒杀单的详细信息...");
            orderService.createSeckillOrder(seckillOrder);
            //手动收单
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
          channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }
}
