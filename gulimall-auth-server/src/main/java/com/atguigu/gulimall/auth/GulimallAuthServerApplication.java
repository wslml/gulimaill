package com.atguigu.gulimall.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

import java.lang.annotation.*;

/**
 * springsession核心原理
 * 1）、@EnableRedisHttpSession导入RedisHttpSessionConfiguration配置
 *  1.给容器中添加一个组件
 *  SessionRepository=>>[RedisOperationsSessionRepository]==>redis操作session.session的增删改查
 *  2.SessionRepositoryFilter==>Filter:session存储过滤器 每个请求过来都必须经过filter
 *      1.创建的时候 就自动从容器中获取到了SessionRepository
 *      2.原始的request response都被包装 SessionRepositoryRquestWrapper SessionRepositoryResponseWrapper
 *      3.以后获取session肯定得通过request.getSession()来获取
 *      4.而原生的request已经被封装成了wrappedRequest 所以这里就会通过wrappedRequest.getSession()来获取session
 *        这个getSession它封装了一下 是从SessionRepository中获取到的 我们给容器中放的是RedisOperationsSessionRepository
 *        所以对session的所有增删改查都是在redis中
 * 用到了spring的装饰者模式
 * session的自动延期考虑到了 redis中的数据也是有过期时间的（用户关闭浏览器后redis中存放的session数据到了过期时间会自动删除）
 */
@EnableRedisHttpSession //整合redis作为session存储
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
//@EnableAutoConfiguration(exclude = {com.alibaba.alicloud.context.oss.OssContextAutoConfiguration.class})
public class GulimallAuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallAuthServerApplication.class, args);
    }

}
