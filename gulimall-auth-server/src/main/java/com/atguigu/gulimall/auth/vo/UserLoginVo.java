package com.atguigu.gulimall.auth.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-11-06 18:03
 */

@Data
public class UserLoginVo {

    private String loginacct;
    private String password;

}
