package com.atguigu.gulimall.member.exception;

/**
 * @author wsl
 * @create 2020-11-05 18:20
 */
public class UsernameExistException extends RuntimeException{
    public UsernameExistException() {
        super("用户名存在");
    }
}
