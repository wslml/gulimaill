package com.atguigu.gulimall.member;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.Md5Crypt;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallMemberApplicationTests {

    //测试md5加密
    @Test
    public void contextLoads() {
        //普通加密 不加盐
        String s = DigestUtils.md5Hex("123456");
        System.out.println(s);

        //盐值加密 随机值 加盐：$1$+8位字符
        String s1 = Md5Crypt.md5Crypt("123456".getBytes(), "$1$qqqqqqqq");
        System.out.println(s1);

        //Spring提供的一种加密算法
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode("123456");
        boolean matches = encoder.matches("123456", encode);
        System.out.println("加密后的密码:"+encode+"是否匹配上:"+matches);
    }

}
