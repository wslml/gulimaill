package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wsl
 * @create 2020-11-17 9:57
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
