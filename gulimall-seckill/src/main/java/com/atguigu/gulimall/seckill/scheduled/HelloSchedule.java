package com.atguigu.gulimall.seckill.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author wsl
 * @create 2020-11-26 10:00
 */

/**
 * 定时任务
 *      1.@EnableScheduling开启定时任务
 *      2.@Scheduled 开启一个定时任务
 *      3.自动配置类 TaskSchedulingAutoConfiguration
 *
 * springboot异步任务
 *      1.@EnableAsync开启异步任务功能
 *      2.@Async 给希望异步执行的方法上标注这个注解
 *      3.自动配置类 TaskExecutionAutoConfiguration 属性绑定在TaskExecutionProperties
 *
 * 解决：使用异步+定时任务来完成定时任务不阻塞的功能
 */
@Slf4j
@Component
//@EnableAsync
//@EnableScheduling
public class HelloSchedule {
    /**
     * spring语法和cron表达式语法的区别
     * 1.spring中6位组成 不允许第7位的年 cron表达式可以有第七位年
     * 2.在周几的位置 1-7代表周一到周日 也可以用英文MON-SUN cron表达式1代表周日
     * 3.定时任务不应该阻塞 默认是阻塞的
     *      1).可以让业务运行以异步的方式 自己提交线程池
     *          CompletableFuture.runAsync(()->{
     *              xxxService.hello();
     *          },executor);
     *      2).支持定时任务线程池 设置TaskSchedulingProperties;
     *          spring.task.scheduling.pool.size=5
     *      3).让定时任务异步执行
     *          异步任务
     */
    @Async
    @Scheduled(cron = "* * * * * ?")
    public void hello() throws InterruptedException {
        log.info("hello...");
        Thread.sleep(3000);
    }
}
