package com.atguigu.gulimall.order.constant;

/**
 * @author wsl
 * @create 2020-11-17 8:15
 */
public class OrderConstant {
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token:";
}
