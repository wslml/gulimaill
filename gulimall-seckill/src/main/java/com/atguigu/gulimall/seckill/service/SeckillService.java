package com.atguigu.gulimall.seckill.service;

import com.atguigu.gulimall.seckill.to.SeckillSkuRedisTo;

import java.util.List;

/**
 * @author wsl
 * @create 2020-11-26 14:36
 */
public interface SeckillService {
    SeckillSkuRedisTo getSkuSeckillInfo(Long skuId);

    void uploadSeckillSkuLates3Days();

    List<SeckillSkuRedisTo> getCurrentSeckillSkus();

    String kill(String killId, String key, Integer num);
}
