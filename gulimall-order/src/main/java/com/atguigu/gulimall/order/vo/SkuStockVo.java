package com.atguigu.gulimall.order.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-11-16 15:55
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
