package com.atguigu.gulimall.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

/**
 * @author wsl
 * @create 2020-11-11 19:55
 */
@Configuration
public class MyRabbitConfig {

    RabbitTemplate rabbitTemplate;

    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory factory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(factory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    /**
     * 使用JSON序列化机制 进行消息转换
     * @return
     */
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplete
     * 1.服务端收到消息就回调
     *      1.开启发送端确认 spring.rabbitmq.publisher-confirms=true
     *      2.设置确认回调
     * 2.消息正常抵达队列回调
     *      1.  spring.rabbitmq.publisher-returns=true
     *          spring.rabbitmq.template.mandatory=true
     *      2.设置确认回调ReturnCallback
     * 3.消费端确认（保证每个消息被正确消费 此时才可以broker删除这个消息）
     *      spring.rabbitmq.listener.simple.acknowledge-mode=manual手动签收配置
     *      1.默认是自动确认的 只要消息接收到 客户端会自动确认 服务端就会移除这个消息
     *          问题：
     *              我们收到很多消息 自动回复给服务器ack 只有一个消息处理成功 宕机了 发生消息丢失
     *              解决：手动确认模式 只要我们没有明确告诉MQ货物被签收 没有Ack 消息就一直是unacked状态
     *              即使Consumer宕机 消息不会丢失 会重新变为ready状态 下一次有新的Consumer连接进来就把消息发给它
     *      2.如何签收
     *          channel.basicAck(deliveryTag,false);签收 业务成功完成就应该签收
     *          channel.basicNack(deliveryTag,false,false);拒签 业务失败 拒签
     */
    //@PostConstruct //MyRabbitConfig对象创建完成后 执行这个方法
    public void initRabbitTemplate(){
        //设置确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             *  1.只要消息抵达Broker就ack=true
             * @param correlationData 当前消息的唯一关联数据（这个消息的唯一Id）
             * @param ack 消息是否成功收到
             * @param cause 消息没有收到的失败原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                /**
                 * 1.做好消息确认机制(publisher,consumer【手动ack】)
                 * 2.每一个发送的消息都在数据库做好记录 定期将失败的消息再次发送一遍
                 */
                //服务器收到消息了
                //修改数据库存的消息状态
                System.out.println("correlationData = " + correlationData + "->ack = " + ack + "->cause = " + cause);
            }
        });
        //设置消息抵达队列的确认回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             *只要消息没有投递给指定的队列 就触发这个失败回调
             * @param message    投递失败的消息详细信息
             * @param replyCode  回复的状态码
             * @param replyText  回复的文本内容
             * @param exchange   当时这个消息发给哪个交换机
             * @param routingKey 当时这个消息用哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                //报错误了 修改数据库当前消息的状态->错误
                System.out.println("Fail message = " + message + "->replyCode = " + replyCode + "->replyText = " + replyText + "->exchange = " + exchange + "->routingKey = " + routingKey);
            }
        });
    }
}
