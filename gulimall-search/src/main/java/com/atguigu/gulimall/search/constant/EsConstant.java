package com.atguigu.gulimall.search.constant;

/**
 * @author wsl
 * @create 2020-10-14 15:23
 */
public class EsConstant {
    public static final String PRODUCT_INDEX = "gulimall_product";//sku数据在es中的索引
    public static final Integer PRODUCT_PAGESIZE = 6;//设置每页显示的数量
}
