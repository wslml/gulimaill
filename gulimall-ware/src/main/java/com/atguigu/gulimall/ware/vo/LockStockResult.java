package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-11-17 14:41
 */
@Data
public class LockStockResult {
    private Long skuId;
    private Integer num;
    private Boolean locked;
}
