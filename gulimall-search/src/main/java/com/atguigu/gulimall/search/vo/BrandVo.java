package com.atguigu.gulimall.search.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-10-30 8:49
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
