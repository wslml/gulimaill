package com.atguigu.gulimall.search.thread;

import org.apache.lucene.search.suggest.document.CompletionsTermsReader;

import java.util.concurrent.*;

/**
 * @author wsl
 * @create 2020-10-30 11:04
 */
public class ThreadTest {
    public static ExecutorService executor = Executors.newFixedThreadPool(10);

    //创建启动异步任务
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*System.out.println("main...start...");
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }, executor);
        System.out.println("main...end...");*/

        /**
         * 方法完成后的感知
         */
        /*
        System.out.println("main...start...");
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 0;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).whenComplete((res,excpiton)->{
            //虽然能得到异常信息，但是没法修改返回数据
            System.out.println("异步任务完成了...结果是:"+res+";异常是:"+excpiton);
        }).exceptionally(throwable -> {
            //可以感知异常，同时返回默认值
            return 10;
        });
        Integer integer = future.get();
        System.out.println("main...end..."+integer);
        */

        /**
         * 方法执行后的处理
         */
        /*
        System.out.println("main...start...");
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 0;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).handle((res,thr)->{
           if (res!=null){
               return res*2;
           }
           if (thr!=null){
               return 0;
           }
           return 0;
        });
        Integer integer = future.get();
        System.out.println("main...end..."+integer);
    }
    */

    /**
    * 线程串行化
     * 1).thenRun:不能获取到上一步的执行结果，无返回值
    */
    /*
    System.out.println("main...start...");
    CompletableFuture.supplyAsync(() -> {
        System.out.println("当前线程：" + Thread.currentThread().getId());
        int i = 10 / 4;
        System.out.println("运行结果：" + i);
        return i;
    }, executor).thenRunAsync(()->{
        System.out.println("任务2启动了...");
    },executor);
        System.out.println("main...end...");
    }
    */

    /**
     * 线程串行化
     * 2).thenAccept:能接受上一步结果，但无返回值
     */
    /*
    System.out.println("main...start...");
    CompletableFuture.supplyAsync(() -> {
        System.out.println("当前线程：" + Thread.currentThread().getId());
        int i = 10 / 4;
        System.out.println("运行结果：" + i);
        return i;
    }, executor).thenAcceptAsync(res->{
        System.out.println("任务2启动了..."+res);
    },executor);
        System.out.println("main...end...");
    }
    */

    /**
     * 线程串行化
     * 3).thenAccept:能接受上一步结果，但无返回值
     */
    /*
    System.out.println("main...start...");
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 4;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).thenApplyAsync(res -> {
            System.out.println("任务2启动了..." + res);
            return "hello" + res;
        });
        System.out.println("main...end..."+future.get());
    }
    */

    /**
     * 两个都完成
     */
        /*CompletableFuture<Object> future01 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务1线程：" + Thread.currentThread().getId());
            int i = 10 / 4;
            System.out.println("任务1结束：" );
            return i;
        }, executor);

        CompletableFuture<Object> future02 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务2开始：" + Thread.currentThread().getId());
            try {
                Thread.sleep(3000);
                System.out.println("任务2结束：");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "hello";
        }, executor);*/

        //不能感知前两个的结果，没有返回值
        /*
        future01.runAfterBothAsync(future02,()->{
            System.out.println("任务3开始...");
        },executor);
        */

        //能感知前两个的结果，但是没有返回值
        /*
        future01.thenAcceptBothAsync(future02,(f1,f2)->{
            System.out.println("任务3开始...之前的结果:"+f1+"---"+f2);
        },executor);
        */

        //能感知前两个的结果，有返回值
        /*
        CompletableFuture<String> future = future01.thenCombineAsync(future02, (f1, f2) -> {
            return f1 + "---" + f2 + "---" + "hahaha";
        });
        System.out.println("结果:"+future.get());
        */
    /**
     * 两个任务，只要有一个完成，我们就完成任务3
     * runAfterEitherAsync:不感知结果，自己也没有返回值
     */
        /*
        future01.runAfterEitherAsync(future02,()->{
            System.out.println("任务3开始...:");
        },executor);
        */
    /**
     * 两个任务，只要有一个完成，我们就完成任务3
     * acceptEitherAsync:能感知结果，自己没有返回值
      */
        /*future01.acceptEitherAsync(future02,(res)->{
            System.out.println("任务3开始:"+res);
        },executor);*/

        /**
         *两个任务，只要有一个完成，我们就完成任务3
         *acceptEitherAsync:能感知结果，自己有返回值
         */
        /*CompletableFuture<String> future = future01.applyToEitherAsync(future02, res ->
        {
            System.out.println("任务3开始...之前的结果:"+res);
            return res.toString() + "---haha";
        }, executor);
        System.out.println("返回结果:"+future.get());*/

        CompletableFuture<String> futureImg = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的图片信息");
            return "hello.jpg";
        });

        CompletableFuture<String> futureAttr = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的属性");
            return "黑色+256G";
        });

        CompletableFuture<String> futureDesc = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品介绍");
            return "华为";
        });

        //必须三个线程都执行完才执行下面的方法
        /*CompletableFuture<Void> allOf = CompletableFuture.allOf(futureImg, futureAttr, futureDesc);
        allOf.get();//等待所有结果执行完成
        System.out.println("main...end..."+futureImg.get()+"---"+futureAttr.get()+"---"+futureDesc.get());*/

        //只要有一个线程执行完成就执行下面的方法
        CompletableFuture<Object> anyOf = CompletableFuture.anyOf(futureImg, futureAttr, futureDesc);
        System.out.println("main...end..."+anyOf.get());

    }

    public void thread(String[] args) {
        //1.继承Thread
        /*
        System.out.println("main...start...");
        Tread01 tread01 = new Tread01();
        tread01.start();//启动线程
        System.out.println("main...end...");
        */

        //2.实现Runnable接口
        /*
        System.out.println("main...start...");
        Runable01 runable01 = new Runable01();
        new Thread(runable01).start();
        System.out.println("main...end...");
        */

        //3.实现Callable接口+FutureTash（可以拿到返回结果，可以处理异常）
        /*
        System.out.println("main...start...");
        FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
        new Thread(futureTask).start();
        //阻塞等待整个线程执行完成，获取返回结果
        try {
            Integer integer = futureTask.get();
            System.out.println("main...end..."+integer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        */
        //我们以后在业务代码里面，以上三种启动线程的方式都不用，上面三种方式相当于公司有个新任务的时候就招聘一个员工
        //有一个任务就开一个线程，我们的资源很快就会被耗尽
        //应该将所有的多线程异步任务都交给线程池执行
        // 这就相当于我们公司有个新活，公司一共有50个人，我们把这个新活交给50个人中的一个来干，如果这个人正好忙着就等他干完手里的活再来干这个新活
        //这样做的好处就是我们达到了资源控制，消耗的资源就是这50个人的资源
        //特别是高并发的业务一定要将所有的异步任务都交给线程池，线程池可以控制资源的消耗
        //new Thread(()-> System.out.println("hello")).start();

        //4.线程池启动
        //当前系统中线程池只设计一两个就行，每个异步任务，提交给线程池让他自己去执行就行
        /*
        //线程池直接提交任务
        System.out.println("main...start...");
        service.execute(new Runable01());
        System.out.println("main...end...");
        */
        //1.创建线程池
            //1).Executors
            //2).new ThreadPoolExecutor
        //future:可以获取到异步结果
        /**
         * 七大参数
         * corePoolSize：
         * 核心线程数【一直存在，除非设置了@code allowCoreThreadTimeOut过期时间，过期了自动销毁】，
         * 线程池创建好以后就准备就绪的线程数量，等待来接受异步任务去执行。
         * maximumPoolSize：最大资源数量；控制资源
         * keepAliveTime：
         * 存活时间。如果当前的线程数量大于core数量。
         * 当线程的空闲时间大于keepAliveTime指定的存活时间时，释放空闲的线程（maximumPoolSize-corePoolSize）
         * unit:时间单位
         * BlockingQueue<Runnable> workQueue：
         * 阻塞队列。如果任务有很多，就会将目前多的任务放在队列里面。只要有线程空闲，就会去队列里面取出新的任务继续执行。
         * threadFactory：线程的创建工厂
         * RejectedExecutionHandler handler：如果队列满了，按照我们指定的拒绝策略拒绝执行任务
         *
         * 工作顺序
         * 1).线程池创建，准备好core数量的核心线程，准备接收任务
         * 1.1.corePoolSize满了，就将再进来的任务放入阻塞队列中。空闲的corePoolSize就会自己去阻塞队列获取任务执行
         * 1.2.阻塞队列满了，就直接开新线程执行，最大只能开到maximumPoolSize指定的数量
         * 1.3.maximumPoolSize满了就用RejectedExecutionHandler拒绝任务
         * 1.4.maximumPoolSize都执行完成，有很多空闲，在指定的时间keepAliveTime以后，释放maximumPoolSize-corePoolSize这些线程
         * 注意：new LinkedBlockingDeque<>()：默认是Integer的最大值。可以自己指定一个值，不然容易造成阻塞队列数量过多内存不够
         *
         * 一个线程池 core:7; max:20; queue:50; 100并发进来是怎么分配的
         * 7个会立即得到执行，50个会进入队列，再开13个进行执行。剩下的30个就使用拒绝策略。
         * 如果不想抛弃还要执行。采用CallerRunsPolicy拒绝策略
         */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        //Executors也能给我们创建线程池 下面是它创建的常用的几种线程池
        //Executors.newCachedThreadPool() core是0，所有线程都可回收
        //Executors.newFixedThreadPool() 固定大小 core=max; 都不回收
        //Executors.newScheduledThreadPool() 定时任务的线程池
        //Executors.newSingleThreadExecutor() 单线程的线程池，后台从队列里面获取任务，挨个执行


        //四种创建线程方式的区别
        //方式1和2不能得到返回值，方式3可以获取返回值
        //方式1.2.3都不能控制资源
        //方式4可以控制资源
        //比如当前系统只有1G内存，顶多200多个线程去执行我们测试除了峰值等等，就直接给线程池里面控制好最大量资源，即使
        //有1000万个任务进来我们也只能200个同时进行，这样可以控制资源，使系统性能稳定，不至于在高并发的情况下资源耗尽
        //稳定是第一位的，高并发再吊，运行10秒系统崩了...
    }

    public static class Tread01 extends Thread{
        @Override
        public void run(){
            System.out.println("当前线程："+Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果："+i);
        }
    }

    public static class Runable01 implements Runnable{
        @Override
        public void run() {
            System.out.println("当前线程："+Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果："+i);
        }
    }

    public static class Callable01 implements Callable<Integer>{
        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程："+Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果："+i);
            return i;
        }
    }
}
