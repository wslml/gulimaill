package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.PmsSpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author wsl
 * @email 526248876@qq.com
 * @date 2020-09-26 20:22:10
 */
@Mapper
public interface PmsSpuInfoDescDao extends BaseMapper<PmsSpuInfoDescEntity> {
	
}
