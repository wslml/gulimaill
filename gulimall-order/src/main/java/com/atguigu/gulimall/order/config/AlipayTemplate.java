package com.atguigu.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atguigu.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2016110200787313";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCT4tqPBSSREnR/7ntH/0btSPvVjjxhK8BlE/WcIL9m09Wu+DeZ5ZSruIcl7hdxeB7kib7zKTL7yXC4X1X9XAoK7o/1sVkJbrwA5EBvDcblCHKnBMKGgllZ6WTUl1IlUea2V8noC7PO6Sr82tmrYiUrK8O7ke62aGC+uhAjDAkGH77ZPCb1jhimj6Awm6LWmM0s7xHMdDLLO5P7QtkTjWDOUbb5edNItoYyKhH3VcZGe2ULIWfqsWGJXJ1/3Xz67p9UlP38HgoklEYZyZ0lsYAhxveszMc3BFyoKYnxI2ONGnzCqSXjfhgEABJIfMglqzWjn2A9Md39I5ddioE1x1YVAgMBAAECggEAaMXc8FzFZl572TM9dmm+d1gwy0EFOBbGLRZx+RYeZJyKFHdY9Rh5Nxv56/59728Z+/I3LZEvxMEXshghFOoDV3Fi3r+yBAPHAJ61o7hQBG3yxK+TnbZ11USMoi0/Dp+y49bYfiTkVCBPy2rFsi1LhgVwm9AtHmBewmGSEzJwpjv47DLpwdiAoC5APjA42/iwlGpR75S8NW+8ZT4I7ZMfh6RiHi5uF1AGcaPsOZH40fRkaMOp1iroX3epXmr1sHt9s1YV+Wz0G+DEe0YkHMekAcKh/9HVEYyxGVYkKLtHlOAIO9GVYMr01H96e2be2179cdxjALWWWBd7O6+lh8jclQKBgQDqrm1QAV9cw8C8JKPxiO01Gt40LjMn4Ha2mxVwAYSesC2bmvmGbrqbfDcaH3ADGw/kklv1Mj5hyd33pk+IlAa/V1bexX7cerdrLzWvTQ85Ti9nUrn8VYznwxZ4biOypImDUG9OPYwVKzNclYArTjKbQIkuT4pPMklwxDoSTKhB7wKBgQChUfuSRz4swA/53ImQhMq0OOMsQ5ztX93CZ0sDBELU/e/UJFMZOr8QpsBv52O3qzIJnBAriTA7fEBNK1io+VyEzpUlbgn5GSNVPWj8xC965PTLQC1aF+GZnW4T9d8Qyzx+vL4gyfxS1F1zqI8GFUjRbgKOdjut4S1xOr8OWtYcOwKBgQC7bnATs+Tr+9PU8BaX89ovzbOmTTfCI33svdPTTwMSknb6jj235nvXbs+7Z1xYvqZk0ZfsLyKvi0BiaEuhM0hoTWMuLOHzSXdwfZYM2ijMhetKREISjGkrOR6bNKwCI3NIk1cyT+mvhn9Q9H4XGC9G27Y0bIZ0Vh9dlvsiXmMLtQKBgE2LiiF7q5B5VnSmJV5Z5tKREeqCKdoVHhVH78/oaXPXCUTxnRxgl92aOqDQGqf8XBYwdblnvc6qYIJ31quLCoUwhJM0fQjHo6ti1KfEaoppqp5VIDfRiPl9qp9frjV1bIQDmGVMksUCHlXtbarf6lv5xLLUdMWVm3F1asestIVnAoGBAKDS5vazOQFBOL2F9ZiMp+O7z748m1Phkk1gFgWlnslZgW8+bxv9u+xVQY1SxJBgggqHml67EEAVSs3zAv5eOCIsojuldt++Gvr1VR3TivVwAYDrm7IFnX+5txcS5X3x4pPyjJzXDyxo16h6qAqwlfrEmpWaT5XMiZEjNf/3vfhk";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtMuqT29xiavrudKZwZoQyN7baoJkShC+ae76Y4p5jk60f+C7K8RNJbo+v8GoZB7upg4VIPPB/RyJ/RDhOc6W9ygFSuthoVWW6dua+Rk5msREDMrAENJIBmqnHp2k+kQaaxH7aY4WJ5PxnjI4juV/hgDlreg3RfvvargeQ8gCRX+2J52i5BEumvMPoDTGh65p8NBrguPldv+o8YSFuLNXN/V9OlhpBESexQOM5h45hgCpHhrnrzKUKYOoLo/yUwZ+iH15/veD43zhB2xkli+nKYOOP3JuTq2XeoJ2IVOksZQufAGh2+xwxauCtAkDZaLbSXbbENdgs74A7jOyT0hX8wIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息 花生壳做的内网穿透
    private  String notify_url = "http://l2n8286552.wicp.vip/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
