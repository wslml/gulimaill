package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author wsl
 * @create 2020-11-17 14:33
 */
@Data
public class WareSkuLockVo {
    private String orderSn;//订单号
    private List<OrderItemVo> locks;//需要锁定的所有库存信息
}
