package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.PmsCommentReplayEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价回复关系
 * 
 * @author wsl
 * @email 526248876@qq.com
 * @date 2020-09-26 20:22:10
 */
@Mapper
public interface PmsCommentReplayDao extends BaseMapper<PmsCommentReplayEntity> {
	
}
