package com.atguigu.gulimall.cart.vo;

/**
 * @author wsl
 * @create 2020-11-10 9:14
 */

import java.math.BigDecimal;
import java.util.List;

/**
 * 整个购物车
 * 需要计算的属性 必须重写他的get方法 保证每次获取属性都会进行计算
 */
public class Cart {

    List<CartItem> items;

    private Integer countNum;//商品数量

    private Integer countType;//商品类型数量

    private BigDecimal totalAmount;//商品总价

    private BigDecimal reduce = new BigDecimal("0.00");//减免价格

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public void setCountType(Integer countType) {
        this.countType = countType;
    }

    public Integer getCountNum() {
        int count = 0;
        if (items!=null && items.size()>0){
            for (CartItem cartItem : items) {
                count += cartItem.getCount();
            }
        }
        return count;
    }

    public void setCountNum(Integer countNum) {
        this.countNum = countNum;
    }

    public Integer getCountType() {
        int count = 0;
        if (items!=null && items.size()>0){
            for (CartItem cartItem : items) {
                count += 1;
            }
        }
        return count;
    }

    public BigDecimal getTotalAmount() {
        BigDecimal bigDecimal = new BigDecimal("0");
        //计算购物项总价
        int count = 0;
        if (items!=null && items.size()>0){
            for (CartItem cartItem : items) {
                if (cartItem.getCheck()){
                    bigDecimal = bigDecimal.add(cartItem.getTotalPrice());
                }
            }
        }
        //减去优惠总价
        BigDecimal subtract = bigDecimal.subtract(getReduce());
        return subtract;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
