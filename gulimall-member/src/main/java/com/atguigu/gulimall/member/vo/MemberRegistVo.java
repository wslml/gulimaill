package com.atguigu.gulimall.member.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author wsl
 * @create 2020-11-05 17:57
 */

@Data
public class MemberRegistVo {

    private String userName;

    private String password;

    private String phone;

}
