package com.atguigu.gulimall.member.exception;

/**
 * @author wsl
 * @create 2020-11-05 18:19
 */
public class PhoneExistException extends RuntimeException{

    public PhoneExistException() {
        super("手机号存在");
    }

}
