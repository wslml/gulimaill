package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author wsl
 * @create 2020-11-06 18:13
 */

@Data
public class MemberLoginVo {

    private String loginacct;
    private String password;

}
