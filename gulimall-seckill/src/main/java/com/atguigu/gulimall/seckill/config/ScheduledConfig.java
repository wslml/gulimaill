package com.atguigu.gulimall.seckill.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author wsl
 * @create 2020-11-26 14:27
 */
@EnableAsync
@EnableScheduling
@Configuration
public class ScheduledConfig {

}
