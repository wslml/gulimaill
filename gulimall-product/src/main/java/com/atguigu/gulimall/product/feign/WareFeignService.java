package com.atguigu.gulimall.product.feign;

import com.atguigu.common.to.SkuHasStockVo;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author wsl
 * @create 2020-10-14 14:11
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {
    //查询sku是否有库存
    @PostMapping("/ware/waresku/hasstock")
    R getSkusHashStock(@RequestBody List<Long> skuIds);
}
