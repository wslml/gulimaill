package com.atguigu.gulimall.cart.vo;

import lombok.Data;
import lombok.ToString;
import sun.dc.pr.PRError;

/**
 * @author wsl
 * @create 2020-11-10 10:24
 */

@ToString
@Data
public class UserInfoTo {

    private Long userId;
    private String userKey;
    //如果cookie里面有临时用户置为true
    private boolean tempUser = false;

}
