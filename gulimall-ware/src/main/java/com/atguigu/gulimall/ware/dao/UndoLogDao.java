package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author wsl
 * @email 526248876@qq.com
 * @date 2020-09-26 20:55:48
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
